﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="XML_SAT._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <br />
    <table width="100%" border="0">
        <tr>
            <td width="49%" valign="top">
                <table width="100%" border="1" cellspacing="0" bordercolordark="white" bordercolorlight="black">
                    <tr>
                        <td>
                            <table width="100%" border="0" cellspacing="1" cellpadding="1">
                                <tr>
                                    <td><strong><font color="#2745a5">Emisor: </font></strong>
                                        <asp:Label ID="Emisor" runat="server" Text="Label"></asp:Label>
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><strong><font color="#2745a5">RFC:</font></strong>
                                        <asp:Label ID="RFC" runat="server" Text="Label"></asp:Label>
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><b>Calle </b>
                                        <asp:Label ID="Calle" runat="server" Text="Label"></asp:Label><b> No. Exterior </b>
                                        <asp:Label ID="noExterior" runat="server" Text="Label"></asp:Label><b> No. Interior </b>
                                        <asp:Label ID="noInterior" runat="server" Text="Label"></asp:Label><b> Colonia </b>
                                        <asp:Label ID="Colonia" runat="server" Text="Label"></asp:Label><b> Localidad </b>
                                        <asp:Label ID="Localidad" runat="server" Text="Label"></asp:Label><b> Del/Municipio </b>
                                        <asp:Label ID="Municipio" runat="server" Text="Label"></asp:Label><b> País </b>
                                        <asp:Label ID="Pais" runat="server" Text="Label"></asp:Label><b> C.P. </b>
                                        <asp:Label ID="CP" runat="server" Text="Label"></asp:Label></td>
                                    <td>&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%" border="0" cellspacing="1" cellpadding="1">
                                <tr>
                                    <td><strong><font color="#2745a5">Receptor: </font></strong>
                                        <asp:Label ID="Receptor" runat="server" Text="Label"></asp:Label></td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><strong><font color="#2745a5">RFC:</font></strong>
                                        <asp:Label ID="rrfc" runat="server" Text="Label"></asp:Label></td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><b>Calle </b>
                                        <asp:Label ID="rcalle" runat="server" Text="Label"></asp:Label><b> No. Exterior </b>
                                        <asp:Label ID="rnoexterior" runat="server" Text="Label"></asp:Label><b> Colonia </b>
                                        <asp:Label ID="rcolonia" runat="server" Text="Label"></asp:Label><b> Del/Municipio </b>
                                        <asp:Label ID="rmunicipio" runat="server" Text="Label"></asp:Label><b> Localidad </b>
                                        <asp:Label ID="rlocalidad" runat="server" Text="Label"></asp:Label><b> Estado </b>
                                        <asp:Label ID="restado" runat="server" Text="Label"></asp:Label><b> País </b>
                                        <asp:Label ID="rpais" runat="server" Text="Label"></asp:Label><b> C.P. </b>
                                        <asp:Label ID="rcp" runat="server" Text="Label"></asp:Label></td>
                                    <td>&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
            <td width="51%" valign="top">
                <table width="100%" border="0">
                    <tr>
                        <td><b><font color="#2745a5">Folio Fiscal:</font></b>
                            <asp:Label ID="foliofiscal" runat="server" Text="Label"></asp:Label></td>
                    </tr>
                    <tr>
                        <td><strong><font color="#2745a5">Serie y Folio:</font><font color="#c00000"><asp:Label ID="serie" runat="server" Text="Label"></asp:Label> - <asp:Label ID="folio" runat="server" Text="Label"></asp:Label></font></strong></td>
                    </tr>
                    <tr>
                        <td><strong><font color="#2745a5">No de Serie del CSD:</font></strong>
                            <asp:Label ID="csd" runat="server" Text="Label"></asp:Label></td>
                    </tr>
                    <tr>
                        <td><strong><font color="#2745a5">Lugar, Fecha y hora de emisión:</font></strong>
                            <asp:Label ID="lugarfecha" runat="server" Text="Label"></asp:Label></td>
                    </tr>
                    <tr>
                        <td><strong><font color="#2745a5">Efecto del Comprobante:</font></strong>
                            <asp:Label ID="efecto" runat="server" Text="Label"></asp:Label></td>
                    </tr>
                    <tr>
                        <td><strong><font color="#2745a5">Régimen Fiscal:</font></strong>
                            <asp:Label ID="regimenfiscal" runat="server" Text="Label"></asp:Label></td>
                    </tr>
                    <tr>
                        <td><strong><font color="#2745a5">No de Serie del Certificado del SAT:</font></strong>
                            <asp:Label ID="nocertificado" runat="server" Text="Label"></asp:Label></td>
                    </tr>
                    <tr>
                        <td><strong><font color="#2745a5">Fecha y hora de certificación:</font></strong>
                            <asp:Label ID="fechahoracertificado" runat="server" Text="Label"></asp:Label></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <table width="100%" border="1" cellspacing="0" bordercolordark="white" bordercolorlight="black">
                    <tr>
                        <th width="9%" scope="col"><font color="#2745a5">Cantidad</font></th>
                        <th width="11%" scope="col"><font color="#2745a5">Unidad</font></th>
                        <th width="16%" scope="col"><font color="#2745a5">Código</font></th>
                        <th width="41%" scope="col"><font color="#2745a5">Descripción</font></th>
                        <th width="10%" scope="col"><font color="#2745a5">Precio Unitario</font></th>
                        <th width="13%" scope="col"><font color="#2745a5">Importe</font></th>
                    </tr>
                    <tr>
                        <td align="right">1</td>
                        <td>No aplica</td>
                        <td>&nbsp;</td>
                        <td>PAGO DE NOMINA</td>
                        <td align="right">2,412.80</td>
                        <td align="right">2,412.80</td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <table width="100%" border="0">
                                <tr>
                                    <td><b><font color="#2745a5">Motivo del descuento:</font></b></td>
                                </tr>
                                <tr>
                                    <td><strong><font color="#2745a5">Moneda:</font></strong>Pesos Mexicanos</td>
                                </tr>
                                <tr>
                                    <td><strong><font color="#2745a5">Tipo de Cambio:</font></strong>1.00</td>
                                </tr>
                                <tr>
                                    <td><strong><font color="#2745a5">Forma de Pago:</font></strong>03</td>
                                </tr>
                                <tr>
                                    <td><strong><font color="#2745a5">Método de Pago:</font></strong>Pago en una sola exhibicion</td>
                                </tr>
                                <tr>
                                    <td><strong><font color="#2745a5">Número de Cuenta de Pago:</font></strong>No identificada</td>
                                </tr>
                                <tr>
                                    <td><strong><font color="#2745a5">Condiciones de Pago:</font></strong>semanal</td>
                                </tr>
                            </table>
                        </td>
                        <td colspan="2" valign="top">
                            <table width="100%" border="0">
                                <tr>
                                    <th width="45%" scope="row" align="left">Subtotal</th>
                                    <td width="55%" align="right">2,412.80</td>
                                </tr>
                                <tr>
                                    <th scope="row" align="left">Descuento</th>
                                    <td align="right">0.00</td>
                                </tr>
                                <tr>
                                    <th scope="row" align="left">Total</th>
                                    <td align="right">2,184.59</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <p><strong><font color="#2745a5">Registro Patronal:</font></strong>24080992100</p>
    <table width="100%" border="1" cellspacing="0" bordercolordark="white" bordercolorlight="black">
        <tr>
            <td><strong><font color="#2745a5">Nombre del Trabajador:</font></strong></td>
            <td><strong><font color="#2745a5">NSS:</font></strong></td>
            <td><strong><font color="#2745a5">RFC:</font></strong></td>
            <td><strong><font color="#2745a5">CURP:</font></strong></td>
            <td><strong><font color="#2745a5">Fecha de Inicio de Relación Laboral:</font></strong></td>
        </tr>
        <tr>
            <td>CONS LIZARRAGA EDGARDO</td>
            <td>24066100173</td>
            <td>COLE610106RW6</td>
            <td>COLE610106HSRNDZ06</td>
            <td>02/03/2007 12:00:00 a. m.</td>
        </tr>
        <tr>
            <td><strong><font color="#2745a5">Fecha Inicial del Pago:</font></strong></td>
            <td><strong><font color="#2745a5">Fecha Final del Pago:</font></strong></td>
            <td><strong><font color="#2745a5">Días Pagados:</font></strong></td>
            <td><strong><font color="#2745a5">Fecha de Pago:</font></strong></td>
            <td><strong><font color="#2745a5">Períodicidad del Pago</font></strong></td>
        </tr>
        <tr>
            <td>26/12/2016 12:00:00 a. m.</td>
            <td>01/01/2017 12:00:00 a. m.</td>
            <td>7.00</td>
            <td>06/01/2017 12:00:00 a. m.</td>
            <td>SEMANAL</td>
        </tr>
        <tr>
            <td><strong><font color="#2745a5">Salario Base de Cotización:</font></strong></td>
            <td><strong><font color="#2745a5">Salario Base Integrado:</font></strong></td>
            <td><strong><font color="#2745a5">Departamento:</font></strong></td>
            <td><strong><font color="#2745a5">Puesto:</font></strong></td>
            <td><strong><font color="#2745a5">Antiguedad:</font></strong></td>
        </tr>
        <tr>
            <td>261.38</td>
            <td>274.63</td>
            <td>CHOFER</td>
            <td></td>
            <td>469</td>
        </tr>
    </table>
    <strong><font color="#2745a5">Percepciones:</font></strong>
    <table width="100%" border="1" cellspacing="0" bordercolordark="white" bordercolorlight="black">
        <tr>
            <th width="9%" scope="col"><font color="#2745a5">Tipo</font></th>
            <th width="15%" scope="col"><font color="#2745a5">Clave</font></th>
            <th width="40%" scope="col"><font color="#2745a5">Descripción</font></th>
            <th width="18%" scope="col"><font color="#2745a5">Importe Gravado</font></th>
            <th width="18%" scope="col"><font color="#2745a5">Importe Exento</font></th>
        </tr>
        <tr>
            <td align="right">1</td>
            <td>001</td>
            <td>Sueldos, Salarios Rayas y Jornales</td>
            <td align="right">1,829.66</td>
            <td align="right">0.00</td>
        </tr>
        <tr>
            <td align="right">1</td>
            <td>016</td>
            <td>Otros</td>
            <td align="right">583.14</td>
            <td align="right">0.00</td>
        </tr>
        <tr>
            <td colspan="3">&nbsp;</td>
            <td colspan="2" valign="top">
                <table width="100%" border="0">
                    <tr>
                        <th width="50%" scope="row" align="left">Total Gravado</th>
                        <td width="50%" align="right">2,412.80</td>
                    </tr>
                    <tr>
                        <th scope="row" align="left">Total Exento</th>
                        <td align="right">0.00</td>
                    </tr>
                    <tr>
                        <th scope="row" align="left">Total Percepciones</th>
                        <td align="right">2,412.80</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <strong><font color="#2745a5">Deducciones:</font></strong>
    <table width="100%" border="1" cellspacing="0" bordercolordark="white" bordercolorlight="black">
        <tr>
            <th width="9%" scope="col"><font color="#2745a5">Tipo</font></th>
            <th width="15%" scope="col"><font color="#2745a5">Clave</font></th>
            <th width="40%" scope="col"><font color="#2745a5">Descripción</font></th>
            <th width="18%" scope="col"><font color="#2745a5">Importe</font></th>
        </tr>
        <tr>
            <td align="right">2</td>
            <td>001</td>
            <td>Seguridad social</td>
            <td align="right">47.25</td>
        </tr>
        <tr>
            <td align="right">2</td>
            <td>002</td>
            <td>ISR</td>
            <td align="right">180.96</td>
        </tr>
        <tr>
            <td colspan="3">&nbsp;</td>
            <td colspan="2" valign="top">
                <table width="100%" border="0">
                    <tr>
                        <th width="50%" scope="row" align="left">Total Deducciones</th>
                        <td width="50%" align="right">228.21</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table width="100%" border="0">
        <tr>
            <td colspan="2">
                <p>
                    <strong><font color="#2745a5">Total con letra:</font></strong>
                    <br />
                    <span style="height: auto!important; font-weight: normal; color: #666; word-wrap: break-word; text-align: left">DOS MIL CIENTO OCHENTA Y CUATRO PESOS MEXICANOS, 59/100 </span>
                </p>
            </td>
        </tr>
        <tr>
            <td rowspan="2">
                <img src="C:\Users\HP 81883\AppData\Local\Temp\Cfdis\470BFBEB-8D6B-4235-9B9F-5DBDF7154BB5.png" width="104" height="104" /></td>
            <td>
                <p>
                    <strong><font color="#2745a5">Sello digital del CFDI:</font></strong>
                    <br />
                    <span style="height: auto!important; font-weight: normal; color: #666; word-wrap: break-word; text-align: left">lBWl2TUt7jtGtZ1QkV0mqopL8Wmy0quo72RV1xozYIAMBsu40ZjdxjhpMGs5JhsQfm6hrsV+2V+cqnIsgd1k+BU4UyFV+Gr123Sw<br />
                        C0wWENvJBWvGe+DA2Kc4gmM1RdPrQYAQRgKprSKXcq/9r94uXbZzj9b0GiFVz7KnETiAuvY=</span>
                </p>
            </td>
        </tr>
        <tr>
            <td>
                <p>
                    <strong><font color="#2745a5">Sello del SAT:</font></strong>
                    <br />
                    <span style="height: auto!important; font-weight: normal; color: #666; word-wrap: break-word; text-align: left">DkjsLpkrX+N8jj+h24sUnGYGhE3ylyBxNZeyaQ9w4T/nFNd/8CucD6HdNAkOu3HUd22r1F51ZEXfIYaseBtcqS98J6xVgR/5pRRV<br />
                        k9FJ+QPjT/SxHvGL8cKe2PZSJMKeBIGqrfv0Mr7YtrWkr1xgJZbDJHOHu4Yxgouy4OouYyo=</span>
                </p>
            </td>
        </tr>
        <tr>
            <td>
                <p>
                    <strong><font color="#2745a5">Cadena Original del complemento de certificación digital del SAT:</font></strong>
                    <br />
                    <span style="height: auto!important; font-weight: normal; color: #666; word-wrap: break-word; text-align: left">||1.0|470BFBEB-8D6B-4235-9B9F-5DBDF7154BB5|2017-01-04T11:03:45|lBWl2TUt7jtGtZ1QkV0mqopL8Wmy0quo72RV1<br />
                        xozYIAMBsu40ZjdxjhpMGs5JhsQfm6hrsV+2V+cqnIsgd1k+BU4UyFV+Gr123SwC0wWENvJBWvGe+DA2Kc4gmM1RdPrQYAQRgKpr<br />
                        SKXcq/9r94uXbZzj9b0GiFVz7KnETiAuvY=|00001000000300250292||</span>
                </p>
            </td>
        </tr>
    </table>
    <div id="3.3">
        <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
    </div>
    <div id="3.2">
        <asp:Label ID="Label2" runat="server" Text="Label"></asp:Label>
    </div>
</asp:Content>
